require 'minitest/autorun'
require './lib/geo_location'

describe GeoLocation do
  before do
    @loc1 = ["19.0760", "72.8777"]
    @loc2 = ["28.7041", "77.1025"]
  end

  describe "#distance_between" do

    it "returns positive distance between two different co-ordinates" do
      distance = GeoLocation.distance_between(@loc1, @loc2)
      assert distance > 0
    end

    it "returns accurate distance between two co-ordinates" do
      distance = GeoLocation.distance_between(@loc1, @loc2)
      assert_equal 1153, distance.round
    end

    it "returns 0 for distance between same co-ordinates" do
      distance = GeoLocation.distance_between(@loc1, @loc1)
      assert_equal 0, distance
    end

  end
end