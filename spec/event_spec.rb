require 'minitest/autorun'
require './lib/customer'
require './lib/event'

describe Event do
  before do
    @event = Event.new
    @event.venue = ["21.1702", "72.8311"]
    @event.invitation_proximity = 10
    @customers = []
    customers = [
      {name:"nirav", user_id:1, latitude:"21.1926", longitude: "72.7997"},
      {name:"john", user_id:12, latitude:"21.1740", longitude: "72.7937"},
      {name:"lee", user_id:11, latitude:"21.1395", longitude: "72.7707"},
      {name:"sharon", user_id:91, latitude:"35.8654", longitude: "101.9732"},
      {name:"jessica", user_id:10, latitude:"21.1755", longitude: "72.6773"},
      {name:"wayne", user_id:6, latitude:"21.7051", longitude: "72.9959"}
    ]
    customers.each do |c|
      @customers << Customer.new(c)
    end
  end

  it "responds to #invitation_proximity" do
    assert_equal 10, @event.invitation_proximity
  end

  describe "#generate_invitee_list" do
    before do
      @invitation_list = @event.generate_invitee_list(@customers)
    end

    it "should not invite sharon, jessica and wayne" do
      assert @invitation_list.none?{|c| ["sharon","jessica","wayne"].include? c.name}
    end

    it "should invite nirav, john and lee" do
      assert @invitation_list.all?{|c| ["nirav","john","lee"].include? c.name}
    end
  end

  describe "#generate_invitee_list for proximity of 5kms" do
    before do
      @event.invitation_proximity = 5
      @invitation_list = @event.generate_invitee_list(@customers)
    end

    it "should not invite sharon, jessica, wayne and lee" do
      assert @invitation_list.none?{|c| ["sharon","jessica","wayne","lee"].include? c.name}
    end

    it "should only invite 2 customers" do
      assert_equal 2, @invitation_list.size
    end
  end
  
end