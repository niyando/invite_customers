require 'minitest/autorun'
require './lib/customer'

describe Customer do
  before do
    params = {name: "Nirav Gandhi", user_id: 10, latitude: "21.1740", longitude: "72.7937"}
    @customer = Customer.new params
  end

  it "responds to #name" do
    assert_equal "Nirav Gandhi", @customer.name 
  end

  it "responds to #user_id" do
    assert_equal 10, @customer.user_id
  end

  it "responds to #latitude and #longitude" do
    assert (@customer.latitude == "21.1740" && @customer.longitude == "72.7937")
  end

  describe "#distance_from" do

    it "responds to the method" do
      assert @customer.respond_to?(:distance_from)
    end

    it "gives distance of venue from customer's location" do
      assert_equal 3.9, @customer.distance_from(["21.1702", "72.8311"]).round(2)
    end

  end

  
end