class Customer
  require_relative "geo_location"
  attr_accessor :user_id, :name, :latitude, :longitude

  def initialize(params)
    params.each do |key, value|
      setter = "#{key}="
      send(setter, value) if respond_to?(setter.to_sym, false)
    end
  end

  def distance_from(coordinates)
    GeoLocation.distance_between([latitude, longitude], coordinates)
  end

end