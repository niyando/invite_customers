class GeoLocation
  EARTH_RADIUS_IN_KMS = 6370

  def self.distance_between(loc_1, loc_2)
    location_1_in_radians = loc_1.map{|deg| deg_to_radian deg}
    location_2_in_radians = loc_2.map{|deg| deg_to_radian deg}
    EARTH_RADIUS_IN_KMS * central_angle(location_1_in_radians, location_2_in_radians)
  end

  def self.deg_to_radian(deg)
    deg.to_f * Math::PI / 180 
  end

  private


  def self.central_angle(loc_1, loc_2)
    Math.acos((Math.sin(loc_1[0])*Math.sin(loc_2[0])) + (Math.cos(loc_1[0])*Math.cos(loc_2[0])*Math.cos((loc_1[1] - loc_2[1]).abs)))
  end

end