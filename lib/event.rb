class Event
  INTERCOM_OFFICE = ["53.3393", "-6.2576841"]
  attr_accessor :name, :venue, :invitation_proximity

  def initialize
    # defaults
    @venue = INTERCOM_OFFICE
    @name = "Intercom Office Tour"
    @invitation_proximity = 50
  end

  def generate_invitee_list(customers)
    customers.select{|c| (c.distance_from(venue) < invitation_proximity) }
  end

end